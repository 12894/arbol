#include <iostream>

int main() {
	
	int final;
	int y = 1;

	std::cin >>final;
	int x = final - 1;


	for (int j = 1; j <= final; j++) {
		for (int i = 1; i <= x; i++) {
			std::cout << " ";
		}
		--x;
		for (int i=1 ; i <= y; i++) {
			std::cout << "*";
		}
		y += 2;
		std::cout << "\n";
	}
	system("pause");
	return 0;
}